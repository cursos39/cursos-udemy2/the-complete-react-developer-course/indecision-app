# Indecision App

## Run the html

```shell
live-server public
```

## Install global depedencies

```shell
yarn global add babel-cli@6.24.1
```

## Compile the jsx

```shell
babel src/app.js --out-file=public/scripts/app.js --presets=env,react
babel src/app.js --out-file=public/scripts/app.js --presets=env,react --watch
```

For playground: 

```shell
babel src/playground/es6-let-const.js --out-file=public/scripts/app.js --presets=env,react --watch
```

## Webpack

[Documentación][1]
[Devtool documentation.][2]


[1]: https://webpack.js.org/concepts/
[2]: https://webpack.js.org/configuration/devtool/