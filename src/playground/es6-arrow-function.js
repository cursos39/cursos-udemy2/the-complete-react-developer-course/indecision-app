// const square = function (x) {
//   return x * x
// }

// const squareAF = x => x * x

// console.log(square(8))
// console.log(squareAF(8))

// Challenge - Use arrow functions
// getFirstName('Mike Smith) --> "Mike"
// Create regular arrow function
// Create arrow function using shorthand syntax

const getFirstNameLong = name => {
  return name.split(' ')[0]
}

const getFirstName = name => name.split(' ')[0]

console.log(getFirstNameLong('Alberto Eyo'))
console.log(getFirstName('Sandra Martinez'))
