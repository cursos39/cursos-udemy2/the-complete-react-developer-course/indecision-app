var nameVar = 'Andrew'
var nameVar = 'Mike'
console.log('nameVar', nameVar)

let nameLet = 'Jen'
nameLet = 'Julie'
console.log('nameLet', nameLet)

const nameConst = 'Frank'
// nameConst = 'Albert'
console.log('nameConst', nameConst)

function getPetName () {
  const petName = 'Hal'
  return petName
}

const petName = getPetName()
console.log(petName)

const fullName = 'Jen Mead'

if (fullName) {
  const firstName = fullName.split(' ')[0]
  console.log(firstName)
}
