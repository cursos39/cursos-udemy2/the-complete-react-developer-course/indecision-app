import React from 'react'
import Header from './Header.js'
import Action from './Action.js'
import Options from './Options.js'
import AddOption from './AddOption.js'
import OptionModal from './OptionModal.js'

class IndecisionApp extends React.Component {
  state = { options: [] }
  selectedOption = undefined

  handleDeleteOptions = () => {
    this.setState(() => ({ options: [] }))
  }

  handleAddOption = (userOption) => {
    if (!userOption) {
      return 'You have to input valid value'
    }
    if (this.state.options.find(option => option === userOption)) {
      return 'This option already exists'
    }
    this.setState(({ options: prevOptions }) => ({
      options: prevOptions.concat([userOption])
    }))
  }

  handlePick = () => {
    const randomIndexOption = Math.floor(Math.random() * this.state.options.length)
    const randomOption = this.state.options[randomIndexOption]
    this.setState(() => ({ selectedOption: randomOption }))
  }

  handleOKModal = () => {
    this.setState(() => ({ selectedOption: undefined }))
  }

  handleDeleteOption = (optionToRemove) => {
    this.setState((prevState) => ({
      options: prevState.options.filter(option => option !== optionToRemove)
    })
    )
  }

  componentDidMount () {
    try {
      const optionsJson = localStorage.getItem('options')
      const options = JSON.parse(optionsJson)
      if (options) {
        this.setState(() => ({ options }))
      }
    } catch (e) {
      // Do nothing at all
    }
  }

  componentDidUpdate (prevProps, prevState) {
    if (prevState.options.length !== this.state.options.length) {
      const optionsJson = JSON.stringify(this.state.options)
      localStorage.setItem('options', optionsJson)
    }
  }

  render () {
    const subtitle = 'Put your life in the hands of the computer'
    return (
      <div>
        <Header subtitle={subtitle}/>
        <div className="container">
          <Action
            hasOptions={this.state.options.length > 0}
            handlePick={this.handlePick}
          />
          <div className="widget">
            <Options
            options={this.state.options}
            handleDeleteOptions={this.handleDeleteOptions}
            handleDeleteOption={this.handleDeleteOption}
            />
            <AddOption
              handleAddOption={this.handleAddOption}
            />
          </div>
        </div>
        <OptionModal
          selectedOption={this.state.selectedOption}
          handleOKModal={this.handleOKModal}
        />
      </div>
    )
  }
}

export default IndecisionApp
